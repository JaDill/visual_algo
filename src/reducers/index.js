import {combineReducers} from 'redux';

import currentDS from './currentDS';

const rootReducer = combineReducers({currentDS});

export default rootReducer;