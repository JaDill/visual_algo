import {
  SET_CURRENTDS_NAME,
  CLEAR_CURRENTDS,
  SET_CURRENTDS_STEPINDEX,
  SET_CURRENTDS_STEPARRAY,
  SET_CURRENTDS_INFOS,
} from '../constants/common';

const currentDS = (state = {}, {name_resourceId, name_method, steps, stepIndex, script,param, type,}) => {
  switch (type) {
    case SET_CURRENTDS_NAME:
      return {
        name_resourceId,
      };
    case SET_CURRENTDS_INFOS:
      return {
        ...state,
        name_method,
        script,
        param,
      };
    case SET_CURRENTDS_STEPINDEX:
      return {
        ...state,
        stepIndex,
      };
    case SET_CURRENTDS_STEPARRAY:
      return {
        ...state,
        stepIndex,
        steps,
      };
    case CLEAR_CURRENTDS:
      return {};
    default:
      return state;
  }
};

export default currentDS;