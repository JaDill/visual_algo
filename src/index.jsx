import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';
import LocaleRouter from './components/LocaleRouter/LocaleRouter';

import './index.css';

ReactDOM.render((
    <BrowserRouter>
        <LocaleRouter/>
    </BrowserRouter>
), document.getElementById('root'));

registerServiceWorker();
