import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import {LANGUAGES, GetTranslation} from '../../resources/translations';
import CardTags from './components/CardTags';

import './css/TurningCardView.css';

const workspace = 'workspace';

const TurningCardView = ({locale, link, name_resourceId, description_resourceId, frontImageSrc, tags}) => {
  const frontStyles = {
    backgroundImage: frontImageSrc && `url(${frontImageSrc})`,
  };
  return (
    <Link className='card' to={`/${locale}/${workspace}/${link}`}>
      <div className='front' style={frontStyles}>
        <h2>{GetTranslation(name_resourceId, locale)}</h2>
      </div>
      <div className='back'>
        <div className='content'>
          <h2>{GetTranslation(name_resourceId, locale)}</h2>
          <CardTags className='tags' tags={tags} locale={locale}/>
          {description_resourceId && <p className='description'>{GetTranslation(description_resourceId, locale)}</p>}
        </div>
      </div>
    </Link>
  );
};

TurningCardView.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
  link: PropTypes.string.isRequired,
  name_resourceId: PropTypes.string.isRequired,
  description_resourceId: PropTypes.string,
  frontImageSrc: PropTypes.string,
  tags: PropTypes.array,
};

TurningCardView.defaultProps = {
  locale: LANGUAGES[0].short_name,
  description_resourceId: '',
  frontImageSrc: '',
  tags: [],
};

export default TurningCardView;