import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

import {GetTranslation, LANGUAGES} from '../../resources/translations';
import {setCurrentDSInfos, clearCurrentDS} from '../../actions/actionCreator';

import './css/FunctionsBlock.css'

const classes = classNames(
  'functionsBlock',
);

const FunctionsBlock = ({locale, methodList, setCurrentDSInfos}) => (
  <div id='functionsBlock' className={classes}>
    <ul>
      {methodList.map(({name_resourceId, script, method, param}) =>
        (<li key={name_resourceId} className='method'>
          <input name='functionBlock' type='radio' id={name_resourceId} value={name_resourceId} onChange={(e) => {
            const liArray = e.target.parentElement.parentElement.children;
            for (const li of liArray) {
              li.classList.remove('method-active');
            }
            e.target.parentElement.classList.add('method-active');
            setCurrentDSInfos({script, name_method: method, param});
          }} onClick={() => {
            const paramBlock = document.getElementById('paramBlock');
            paramBlock.classList.remove('paramBlock-minimized');
            paramBlock.getElementsByTagName('input').namedItem('minimized').checked = false;
          }}/>
          <label htmlFor={name_resourceId}>{GetTranslation(name_resourceId, locale)}</label>
        </li>)
      )}
    </ul>
    <div className='minimizedMethodList'>
      <input id='minimizedMethodList' name='minimized' type='checkbox' onChange={e => {
        if (e.target.checked) {
          document.getElementById('functionsBlock').classList.add('functionsBlock-minimized');
          const paramBlock = document.getElementById('paramBlock');
          paramBlock.classList.add('paramBlock-minimized');
          paramBlock.getElementsByTagName('input').namedItem('minimized').checked = true;
        } else {
          document.getElementById('functionsBlock').classList.remove('functionsBlock-minimized');
        }
      }}/>
      <label htmlFor='minimizedMethodList'>
        <svg className='arrow'>
          <path d='M8 16 L24 3 L24 29 L8 16'/>
        </svg>
      </label>
    </div>
  </div>
);

FunctionsBlock.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
  methodList: PropTypes.array.isRequired,
};

FunctionsBlock.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default connect(state => ({
  currentScript: state.currentDS.script,
}), {setCurrentDSInfos, clearCurrentDS})(FunctionsBlock);