import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import NoFound from '../NoFound/NoFound';
import PageRouter from '../PageRouter/PageRouter';
import {LANGUAGES} from '../../resources/translations';

const defaultLocale = LANGUAGES[0].short_name;

const LocaleRouter = () => {
  const allVariantsOfLocale = [
    ...LANGUAGES.map(({short_name}) => `/${short_name}`),
    ...LANGUAGES.map(({short_name}) => `/${short_name}/*`),
  ];
  return (
    <Switch>
      <Route exact path='/' render={
        () => (<Redirect to={`/${defaultLocale}/`}/>)
      }/>
      <Route path={allVariantsOfLocale} component={PageRouter}/>
      <Route path='*' component={NoFound}/>
    </Switch>
  );
};

export default LocaleRouter;