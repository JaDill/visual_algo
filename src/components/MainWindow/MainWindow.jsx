import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

import Header from '../Header/Header';

import AlgorithmsList from "../AlgorithmsList/AlgorithmsList";
import {LANGUAGES} from "../../resources/translations";

const MainWindow = (props) => (
  <Fragment>
    <Header {...props}/>
    <AlgorithmsList {...props}/>
    {/*<Footer {...props}/>*/}
  </Fragment>
);

MainWindow.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

MainWindow.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default MainWindow;