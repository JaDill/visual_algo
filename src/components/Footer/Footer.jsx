import React from 'react';

import {GetTranslation} from '../../resources/translations';
import PropTypes from 'prop-types';
import {LANGUAGES} from '../../resources/translations';

const classes = [
    'footer',
];

const Footer = ({locale}) => (
    <footer className={classes}>
        <p>{GetTranslation('footer-with-technologies', locale)}</p>
        <p>{GetTranslation('footer-copyrights', locale)}</p>
    </footer>
);

Footer.propTypes = {
    locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

Footer.defaultProps = {
    locale: LANGUAGES[0].short_name,
};

export default Footer;