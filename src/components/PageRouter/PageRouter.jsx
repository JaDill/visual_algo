import React from 'react';
import {Switch, Route} from 'react-router-dom';

import {LANGUAGES} from '../../resources/translations';
import {DATA_STRUCTURES} from '../../constants/dataStructures';
import NoFound from '../NoFound/NoFound';
import MainWindow from '../MainWindow/MainWindow';
import Workspace from '../Workspace/Workspace';
import store from '../../resources/store';
import {Provider} from 'react-redux';

const PageRouter = () => {
  const langInPath = window.location.pathname.match(`^(${LANGUAGES.map(({short_name}) => `/${short_name}`).join('|')})`);
  let locale = LANGUAGES[0].short_name;
  if (langInPath !== null) {
    locale = langInPath[0].replace('/', '');
  }
  return (
    <Provider store={store}>
      <Switch>
        <Route exact path={`/${locale}`} render={() => (<MainWindow locale={locale}/>)}/>
        <Route exact path={`/${locale}/about`} render={() => (<MainWindow locale={locale}/>)}/>
        {
          DATA_STRUCTURES.map(item => (
            <Route key={item.link} path={`/${locale}/workspace/${item.link}`}
                   render={() => (<Workspace locale={locale} dataStructure={item}/>)}/>
          ))
        }
        <Route path='*' render={() => (<NoFound locale={locale}/>)}/>
      </Switch>
    </Provider>
  );
};

export default PageRouter;