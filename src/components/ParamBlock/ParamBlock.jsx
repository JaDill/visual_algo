import classNames from 'classnames';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {GetTranslation, LANGUAGES} from '../../resources/translations';
import {RedBlackTree} from '../../resources/dataStructures/rbt';
import {BTree} from '../../resources/dataStructures/bt';
import {Treap} from '../../resources/dataStructures/treap';
import {setCurrentDSStepsArray} from '../../actions/actionCreator';
import {NUMBER, RADIO, FILE} from '../../constants/common';

import './css/ParamBlock.css'

const classes = classNames(
  'paramBlock',
  'paramBlock-minimized',
);

class ParamBlock extends Component {
  constructor(props) {
    super(props);
    const {nameDS} = props;
    const tree = this.createTree(nameDS);
    this.state = {
      tree,
    };
  }

  createTree(nameDS) {
    switch (nameDS) {
      case 'rbtree_name':
        return new RedBlackTree();
      case 'btree_name':
        return new BTree(3);
      case 'cartesiantree_name':
        return new Treap();
      default:
        return;
    }
  }

  createParamInput(type, values) {
    const id = 'paramBlock-input';
    switch (type) {
      case NUMBER:
        return <input type='number' id={id}/>;
      case RADIO:
        const temp = [];
        for (let value of values) {
          temp.push(<label key={value} className='radioGroup'><input type='radio' name={id} className={id}
                                                                     value={value}/>{value}</label>);
        }
        return temp;
      case FILE:
        return <input type='file' id={id} accept='.json'/>
      default:
        return;
    }
  }

  callTreeMethod(method, value, setCurrentDSStepsArray, withoutIf = false) {
    if (value || withoutIf) {
      const {tree} = this.state;
      const scriptBlock = document.getElementById('scriptBlock');
      scriptBlock.classList.remove('scriptBlock-minimized');
      scriptBlock.getElementsByTagName('input').namedItem('minimized').checked = false;

      // const functionsBlock = document.getElementById('functionsBlock');
      // functionsBlock.classList.add('functionsBlock-minimized');
      // functionsBlock.getElementsByTagName('input').namedItem('minimized').checked = true;
      //
      // const paramBlock = document.getElementById('paramBlock');
      // paramBlock.classList.add('paramBlock-minimized');
      // paramBlock.getElementsByTagName('input').namedItem('minimized').checked = true;

      (tree[method])(value);
      setCurrentDSStepsArray(tree.steps);
      this.setState({tree});
    }
  }

  render() {
    const {locale, method, setCurrentDSStepsArray, nameDS, param, steps, stepIndex} = this.props;
    let {tree} = this.state;
    if (!tree) {
      tree = this.createTree(nameDS);
      this.setState({tree,});
    }
    return (
      <div id='paramBlock' className={classes}>
        {this.createParamInput(param?.type, param?.values)}
        <button className='paramBlock-apply'
                disabled={!method || ((steps?.length ?? 1) !== (stepIndex ?? 0) + 1 && (steps?.length ?? 0) !== (stepIndex ?? 0))}
                onClick={() => {
                  let value;
                  switch (param?.type) {
                    case NUMBER:
                      value = parseInt(document.getElementById('paramBlock-input').value);
                      if (value < 1 || value > 2 ** 31 - 1) {
                        alert(GetTranslation('restrictionChecker_valueBeOnlyPositive', locale));
                        return;
                      }
                      break;
                    case FILE:
                      const reader = new FileReader();
                      const input = document.getElementById('paramBlock-input');
                      reader.onload = e => {
                        this.callTreeMethod(method, e.target.result, setCurrentDSStepsArray);
                      }
                      reader.readAsBinaryString(input.files[0]);
                      input.value = null;
                      break;
                    case RADIO:
                      const radioGroup = document.getElementsByClassName('paramBlock-input');
                      for (let radio of radioGroup) {
                        if (radio.checked) {
                          value = parseInt(radio.value);
                          break;
                        }
                      }
                      break;
                    default:
                      value = null;
                      this.callTreeMethod(method, value, setCurrentDSStepsArray, true);
                  }
                  this.callTreeMethod(method, value, setCurrentDSStepsArray);
                }}>{GetTranslation('run', locale)}
        </button>
        <div className='minimizedParam'>
          <input id='minimizedParam' name='minimized' type='checkbox' defaultChecked={true} onChange={e => {
            if (e.target.checked) {
              document.getElementById('paramBlock').classList.add('paramBlock-minimized');
            } else {
              document.getElementById('paramBlock').classList.remove('paramBlock-minimized');
              const functionsBlock = document.getElementById('functionsBlock');
              functionsBlock.classList.remove('functionsBlock-minimized');
              functionsBlock.getElementsByTagName('input').namedItem('minimized').checked = false;
            }
          }}/>
          <label htmlFor='minimizedParam'>
            <svg className='arrow'>
              <path d='M8 16 L24 3 L24 29 L8 16'/>
            </svg>
          </label>
        </div>
      </div>
    )
  }
}

ParamBlock.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

ParamBlock.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default connect(state => ({
  method: state.currentDS.name_method,
  nameDS: state.currentDS.name_resourceId,
  param: state.currentDS.param,
  steps: state.currentDS.steps,
  stepIndex: state.currentDS.stepIndex,
}), {setCurrentDSStepsArray})(ParamBlock);