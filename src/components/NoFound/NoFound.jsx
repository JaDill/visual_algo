import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import {GetTranslation, LANGUAGES} from "../../resources/translations";

import './css/NoFound.css';

const NoFound = ({locale}) => (
  <div className='no-found_container'>
    <div>
      <h2>{GetTranslation('no-found_container_header', locale)}</h2>
      <h1>404</h1>
      <p>{GetTranslation('no-found_container_description', locale)}</p>
      <Link to={`/${locale}`}>{GetTranslation('no-found_container_link-home', locale)}</Link>
    </div>
  </div>
);

NoFound.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

NoFound.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default NoFound;