import classNames from 'classnames';
import React from 'react';
import Viz from 'viz.js';
import {Module, render} from 'viz.js/full.render'
import {connect} from 'react-redux';
import svgPanZoom from 'svg-pan-zoom';

import './css/RenderBlock.css';

const classes = classNames(
  'renderBlock',
);

const RenderBlock = ({steps, stepIndex}) => {
  let viz = new Viz({Module, render});
  if (steps && steps.length) {
    viz.renderSVGElement(steps[stepIndex].graph)
      .then(result => {
        const parent = document.getElementById('renderBlock');
        parent.innerHTML = '';
        parent.append(result);
        result.style = 'position:absolute;left:0;top:1.83rem;bottom:2rem;width:100%;height:calc(100% - 3.83rem);';
        svgPanZoom(result, {
            controlIconsEnabled: true,
          contain: true,
          }
        );
      })
      .catch(error => {
        // Create a new Viz instance (@see Caveats page for more info)
        viz = new Viz({Module, render});

        // Possibly display the error
        console.error(error);
      });
  }
  return (
    <div id='renderBlock' className={classes}/>
  );
};

export default connect(state => ({
  steps: state.currentDS.steps,
  stepIndex: state.currentDS.stepIndex,
}))(RenderBlock);