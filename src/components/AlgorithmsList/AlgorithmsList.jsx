import React from 'react';
import PropTypes from 'prop-types';

import {LANGUAGES} from '../../resources/translations';
import {DATA_STRUCTURES} from '../../constants/dataStructures';

import './css/AlgorithmsList.css';
import CardViewListItem from "./components/CardViewListItem";

const classes = [
  'algorithms-list',
];

const AlgorithmsList = (props) => (
  <main>
    <ul className={classes}>
      {DATA_STRUCTURES.map((dataStruct) => (
        <li key={dataStruct.key} className='card'>
          <CardViewListItem {...props} dataStructure={dataStruct}/>
        </li>
      ))}
    </ul>
  </main>
);

AlgorithmsList.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

AlgorithmsList.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default AlgorithmsList;