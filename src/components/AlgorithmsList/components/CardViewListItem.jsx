import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import {LANGUAGES, GetTranslation} from '../../../resources/translations';

import './css/CardViewListItem.css'

const workspace = 'workspace';

const CardViewListItem = ({locale, dataStructure: {link, name_resourceId, svg}}) => {
  return (
    <Link to={`/${locale}/${workspace}/${link}`}>
      <h2 className='title'>{GetTranslation(name_resourceId, locale)}</h2>
      <div className='bar'>
        <div className='empty-bar'/>
        <div className='filled-bar'/>
      </div>
      <svg width='208' height='170' dangerouslySetInnerHTML={{__html: svg}}>
      </svg>
    </Link>
  );
};

CardViewListItem.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
  dataStructure: PropTypes.shape({
    link: PropTypes.string.isRequired,
    name_resourceId: PropTypes.string.isRequired,
  }).isRequired,
};

CardViewListItem.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default CardViewListItem;