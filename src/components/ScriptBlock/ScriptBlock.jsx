import classNames from 'classnames';
import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import './css/ScriptBlock.css'

const classes = classNames(
  'scriptBlock',
  'scriptBlock-minimized',
);

const ScriptBlock = ({currentScript, steps, stepIndex}) => (
  <div id='scriptBlock' className={classes}>
    <div className='minimizedScript'>
      <input id='minimizedScript' name='minimized' type='checkbox' defaultChecked={true} onChange={e => {
        if (e.target.checked) {
          document.getElementById('scriptBlock').classList.add('scriptBlock-minimized');
        } else {
          document.getElementById('scriptBlock').classList.remove('scriptBlock-minimized');
        }
      }}/>
      <label htmlFor='minimizedScript'>
        <svg className='arrow'>
          <path d='M8 16 L24 3 L24 29 L8 16'/>
        </svg>
      </label>
    </div>
    <ul>
      {currentScript && currentScript.map(({id, nesting, value}) => (
        <li key={id} id={`script_line${id}`} style={{paddingLeft: `calc(0.4rem * ${nesting})`}}
            className={steps && steps[stepIndex]?.line === id ? 'active' : ''}>{value}</li>
      ))}
    </ul>
  </div>
);

ScriptBlock.propTypes = {
  currentScript: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      nesting: PropTypes.number.isRequired,
      value: PropTypes.string.isRequired,
    })
  ),
};

ScriptBlock.defaultProps = {};

export default connect(state => ({
  currentScript: state.currentDS.script,
  steps: state.currentDS.steps,
  stepIndex: state.currentDS.stepIndex,
}))(ScriptBlock);