import React from 'react';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import PropTypes from "prop-types";

import {PROJECT_NAME} from '../../constants/common';
import {GetTranslation, LANGUAGES} from "../../resources/translations";

import './css/Header.css';

const classes = classNames(
  'header',
  'header-tight',
  'width100',
);

const WorkspaceHeader = ({locale, name_resourceId}) => (
  <header className={classes}>
    <h1><Link to={`/${locale}`}>{`${PROJECT_NAME}/${GetTranslation(name_resourceId, locale)}`}</Link></h1>
  </header>
);

WorkspaceHeader.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

WorkspaceHeader.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default WorkspaceHeader;