import React from 'react';
import {NavLink} from 'react-router-dom';

import {LANGUAGES} from '../../../resources/translations';

import './LangLink.css'

const classes = [
    'lang-list',
];

const LangLinks = () => (
    <ul className={classes}>
        {LANGUAGES.map(({short_name, name}) => (
            <li key={short_name}>
                <NavLink to={`/${short_name}`}>{name}</NavLink>
            </li>
        ))}
    </ul>
);

export default LangLinks;