import React from 'react';
import PropTypes from 'prop-types'

import './ExtraLinks.css'
import {LANGUAGES} from "../../../resources/translations";

// const extraLinks = [
//     {
//         key: 'ab',
//         link: 'about',
//         resourceName: 'about-site'
//     },
// ];

const classes = [
  'extra-links',
];

const ExtraLinks = (/*{locale}*/) => (
  // <ul className={classes}>
  //     {extraLinks.map(({key, link, resourceName}) => (
  //         <li key={key}>
  //             <Link to={`/${locale}/${link}`}>{GetTranslation(resourceName, locale)}</Link>
  //         </li>
  //     ))}
  // </ul>
  <div className={classes}/>
);

ExtraLinks.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

ExtraLinks.defaultProps = {
  locale: LANGUAGES[0].short_name,
};


export default ExtraLinks;