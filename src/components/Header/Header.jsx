import React from 'react';
import classNames from 'classnames';
import {Link} from 'react-router-dom';

import LangLinks from './components/LangLinks';
import ExtraLinks from './components/ExtraLinks';
import {PROJECT_NAME} from '../../constants/common';

import './css/Header.css';
import PropTypes from "prop-types";
import {LANGUAGES} from "../../resources/translations";

const classes = classNames(
  'header',
  'width100',
);

const Header = ({locale}) => (
  <header className={classes}>
    <LangLinks/>
    <h1><Link to={`/${locale}`}>{PROJECT_NAME}</Link></h1>
    <ExtraLinks locale={locale}/>
  </header>
);

Header.propTypes = {
  locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
};

Header.defaultProps = {
  locale: LANGUAGES[0].short_name,
};

export default Header;