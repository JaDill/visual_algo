import classNames from 'classnames';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {setCurrentDSStepIndex} from '../../actions/actionCreator';

import './css/StepManagementBlock.css';
import './css/fontello.css';
import './css/animation.css';
import './css/fontello-embedded.css';
import './css/fontello-codes.css';
import './css/fontello-ie7-codes.css';
import './css/fontello-ie7.css';

const classes = classNames(
  'stepManagementBlock',
);

class StepManagementBlock extends Component {
  state = {
    isInPause: false,
    timerId: null,
  };

  render() {
    let {steps = [], setCurrentDSStepIndex, stepIndex = 0} = this.props;
    const {isInPause, timerId} = this.state;
    return (
      <div className={classes}>
        <input type="range" min="200" max="1000" defaultValue='300' step='10' id='stepManagementBlock-timeout'
               onChange={() => {
                 if (isInPause) {
                   const pauseButton = document.getElementById('pause');
                   pauseButton.click();
                 }
               }}/>
        <button id='toBegin' title='To begin' disabled={stepIndex <= 0} onClick={() => {
          setCurrentDSStepIndex(0);
        }}>
          <i className="icon-to-start" aria-disabled='true'/>
        </button>
        <button id='stepBackward' title='Step backward' disabled={stepIndex <= 0} onClick={() => {
          if (stepIndex - 1 >= 0) {
            setCurrentDSStepIndex(stepIndex - 1);
          }
        }}>
          <i className="icon-fast-bw" aria-disabled='true'/>
        </button>
        <button id='play' title='Play' disabled={isInPause || steps.length < 2} onClick={() => {
          const timerId = setInterval(() => {
            if (!isInPause && stepIndex + 1 < steps.length) {
              setCurrentDSStepIndex(++stepIndex);
            } else {
              clearInterval(timerId);
              this.setState({isInPause: false, timerId: null});
            }
          }, document.getElementById('stepManagementBlock-timeout').value);
          this.setState({isInPause: true, timerId});
        }}>
          <i className="icon-play" aria-disabled='true'/>
        </button>
        <button id='pause' title='Pause' disabled={!isInPause} onClick={() => {
          clearInterval(timerId);
          this.setState({isInPause: false, timerId: null});
        }}>
          <i className="icon-pause" aria-disabled='true'/>
        </button>
        <button id='stepForward' title='Step forward' disabled={stepIndex + 1 >= steps.length} onClick={() => {
          if (stepIndex + 1 < steps.length) {
            setCurrentDSStepIndex(stepIndex + 1);
          }
        }
        }>
          <i className="icon-fast-fw" aria-disabled='true'/>
        </button>
        <button id='toEnd' title='To end' disabled={stepIndex + 1 >= steps.length} onClick={() => {
          setCurrentDSStepIndex(steps.length - 1);
        }}>
          <i className="icon-to-end" aria-disabled='true'/>
        </button>
      </div>
    );
  }
}

StepManagementBlock.propTypes = {
  currentScript: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      nesting: PropTypes.number.isRequired,
      value: PropTypes.string.isRequired,
    })
  ),
};

StepManagementBlock.defaultProps = {};

export default connect(state => ({
  stepIndex: state.currentDS.stepIndex,
  steps: state.currentDS.steps,
}), {setCurrentDSStepIndex})(StepManagementBlock);