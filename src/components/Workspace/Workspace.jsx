import React, {Component, Fragment} from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';

import {LANGUAGES} from '../../resources/translations';
import WorkspaceHeader from '../Header/WorkspaceHeader';
import FunctionsBlock from '../FunctionsBlock/FunctionsBlock';
import CodeBlock from '../ScriptBlock/ScriptBlock';
import RenderBlock from '../RenderBlock/RenderBlock';
import StepManagementBlock from '../StepManagementBlock/StepManagementBlock';
import {clearCurrentDS, setCurrentDSName} from '../../actions/actionCreator';
import ParamBlock from '../ParamBlock/ParamBlock';

import './css/Workspace.css';

class Workspace extends Component{
  static propTypes = {
    locale: PropTypes.oneOf(LANGUAGES.map(({short_name}) => short_name)),
    dataStructure: PropTypes.shape({
      name_resourceId: PropTypes.string.isRequired,
      methodList: PropTypes.arrayOf(PropTypes.shape({
        name_resourceId: PropTypes.string.isRequired,
        script: PropTypes.array.isRequired,
        method: PropTypes.string.isRequired,
      })).isRequired
    }).isRequired,
  };

  static defaultProps = {
    locale: LANGUAGES[0].short_name,
  };

  constructor(props){
    super(props);
    props.clearCurrentDS();
  }

  render(){
    const {locale, dataStructure: {name_resourceId, methodList}, currentDSName, setCurrentDSName} = this.props;
    if (currentDSName !== name_resourceId) {
      setCurrentDSName(name_resourceId);
    }
    return (
      <Fragment>
        <WorkspaceHeader locale={locale} name_resourceId={name_resourceId}/>
        <main>
          <RenderBlock/>
          <FunctionsBlock locale={locale} methodList={methodList}/>
          <ParamBlock locale={locale}/>
          <CodeBlock/>
          <StepManagementBlock/>
        </main>
      </Fragment>
    );
  }
}

export default connect(state => ({
  currentDSName: state.currentDS.name_resourceId,
}), {clearCurrentDS, setCurrentDSName})(Workspace);