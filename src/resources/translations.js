export const LANGUAGES = [
  {
    short_name: 'en',
    name: 'eng',
  },
  {
    short_name: 'ru',
    name: 'rus',
  },
];

const TRANSLATIONS = {
  'no-found_container_header': {
    en: 'Oops! Nothing was found.',
    ru: 'Упс! Ничего не найдено.',
  },
  'no-found_container_description': {
    en: 'We can\'t find the page you\'re looking for.',
    ru: 'Кажется, такой страницы и вовсе не существует.',
  },
  'no-found_container_link-home': {
    en: 'Go back home',
    ru: 'Домой',
  },
  'about-site': {
    en: 'About',
    ru: 'О сайте',
  },
  'footer-with-technologies': {
    en: 'Created with React.js',
    ru: 'Создано на базе React.js'
  },
  'footer-copyrights': {
    en: '© Powered by Jacob Dillinger, 2020',
    ru: '© Сделано Jacob Dillinger, 2020',
  },
  'rbtree_name': {
    en: 'red-black tree',
    ru: 'красно-чёрное дерево',
  },
  'btree_name': {
    en: 'B tree',
    ru: 'B дерево',
  },
  'cartesiantree_name': {
    en: 'treap',
    ru: 'декартово дерево / дерамида',
  },
  'insert': {
    en: 'Insert',
    ru: 'Вставить элемент'
  },
  'search': {
    en: 'Search',
    ru: 'Найти элемент'
  },
  'remove': {
    en: 'Remove',
    ru: 'Удалить элемент'
  },
  'run': {
    en: 'Run',
    ru: 'Пуск',
  },
  'btree_dimensionality': {
    en: 'Change dimensionality',
    ru: 'Изменить мерность',
  },
  'restrictionChecker_valueBeOnlyPositive': {
    en: 'Value must be in range [1, 2^31 - 1]',
    ru: 'Значение должно быть внутри интервала (0, 2^31)',
  },
  'restrictionChecker_btreeDimensionality': {
    en: 'Value must be in range [2, 7]',
    ru: 'Размерность b-дерева должна быть внутри отрезка [2, 7]',
  },
  'import': {
    en: 'Import state',
    ru: 'Импортировать',
  },
  'export': {
    en: 'Export state',
    ru: 'Экспортировать',
  },
  'wrongFile': {
    en: 'Wrong file',
    ru: 'Неверный файл',
  },
  'corruptedFile': {
    en: 'File corrupted',
    ru: 'Файл повреждён',
  },
  'reinsertError': {
    en: 'Reinsertion prohibited',
    ru: 'Повторная вставка запрещена',
  },
  'nothingToRemove': {
    en: 'Nothing to remove',
    ru: 'Удаляемого элемента нет',
  },
};

export const GetTranslation = (resource_name, locale) => {
  if (TRANSLATIONS[resource_name]) {
    if (!locale) {
      locale = GetLocaleFromPathname();
    }
    return TRANSLATIONS[resource_name][locale] ?
      TRANSLATIONS[resource_name][locale] :
      TRANSLATIONS[resource_name][LANGUAGES[0].short_name];
  } else {
    return '';
  }
};

const GetLocaleFromPathname = () => {
  const langInPath = window.location.pathname.match(`^(${LANGUAGES.map(({short_name}) => `/${short_name}/`).join('|')})`);
  return langInPath ? langInPath[0].replace(/\//g, '') : '';
};
