import {GetTranslation} from '../translations';
import {saveAs} from 'file-saver';

export class BTree_Node {
  constructor(t, values = [], pointers = []) {
    this._values = values;
    this._t = t;
    this._pointers = new Array(2 * t).fill(null);
    let i = 0;
    for (let pointer of pointers) {
      this._pointers[i++] = pointer;
    }
    this._parent = null;
  }

  get t() {
    return this._t;
  }

  get maxValues() {
    return this.t * 2 - 1;
  }

  get minValues() {
    return this.t - 1;
  }

  get isLeaf() {
    return this.pointers.filter(elem => !elem).length === this.pointers.length;
  }

  findIndexElement(value) {
    return this._values.findIndex(val => val === value);
  }

  get indexPointer() {
    const pointers = this.parentPage.pointers;
    let i = -1;
    for (let pointer of pointers) {
      ++i;
      if (this === pointer) {
        break;
      }
    }
    return i < pointers.length ? i : -1;
  }

  findIndexPointer(value) {
    let i = 0;
    for (let val of this._values) {
      if (value < val) {
        break;
      }
      ++i;
    }
    return i;
  }

  get parentPage() {
    return this._parent;
  }

  set parentPage(parent) {
    this._parent = parent;
  }

  get values() {
    return this._values;
  }

  clearValuesFromNull() {
    this._values = this.values.filter(elem => elem);
  }

  get pointers() {
    return this._pointers;
  }

  setPointer(childPage, index) {
    this._pointers[index] = childPage;
    if (childPage) {
      childPage._parent = this;
    }
  }

  pushPointer(childPage) {
    for (let i = 0; i < this.pointers.length; ++i) {
      if (!this.pointers[i]) {
        this.pointers[i] = childPage;
        break;
      }
    }
    if (childPage) {
      childPage._parent = this;
    }
  }

  slice() {
    const newLeft = new BTree_Node(this.t, this.values.slice(0, this.t - 1), this.pointers.slice(0, this.t));
    for (let pointer of newLeft.pointers.filter(elem => elem)) {
      pointer.parentPage = newLeft;
    }
    const newRight = new BTree_Node(this.t, this.values.slice(this.t), this.pointers.slice(this.t));
    for (let pointer of newRight.pointers.filter(elem => elem)) {
      pointer.parentPage = newRight;
    }
    return {newLeft, medianValue: this.values[this.t - 1], newRight, parent: this.parentPage};
  }

  merge(brotherPage, medianValueInParentIndex, isBrotherLeft = true) {
    const newValues = [];
    if (isBrotherLeft) {
      for (let item of brotherPage.values) {
        newValues.push(item);
        this.pointers.unshift(null);
        this.pointers.pop();
      }
      newValues.push(this.parentPage.values[medianValueInParentIndex]);
      this.pointers.unshift(null);
      this.pointers.pop();
      for (let item of this.values.filter(elem => elem)) {
        newValues.push(item);
      }
    } else {
      for (let item of this.values.filter(elem => elem)) {
        newValues.push(item);
      }
      newValues.push(this.parentPage.values[medianValueInParentIndex]);
      for (let item of brotherPage.values) {
        newValues.push(item);
      }
    }
    this._values = newValues;
  }

  insertElement(value) {
    const indexPointer = this.findIndexPointer(value);
    this.values.splice(indexPointer, 0, value);
    this.pointers.splice(indexPointer, 0, null);
    this.pointers.pop();
    return indexPointer;
  }

  needNormalizeOverflowing() {
    return this.values.length >= this.maxValues;
  }

  needNormalizeThin() {
    return this.values.length < this.minValues;
  }

  get canSteal() {
    return this.values.length > this.minValues;
  }
}

export class BTree_TestHelper {
  dfs(page, height, currentHeight = 0, parent = null, pointerIndex = 0) {
    if (!page) {
      return height === currentHeight;
    }
    ++currentHeight;
    // совпадение размерности
    if (page.parentPage !== parent || (parent && page.t !== parent.t)) {
      return false;
    }
    // количество в странице
    if (this.treeRoot !== page && (page.values.length < page.minValues || page.values.length > page.maxValues)) {
      return false;
    }
    // не лист, значит элементов должно быть на 1 меньше указателей
    if (!page.isLeaf && page.values.length + 1 !== page.pointers.filter(elem => elem).length) {
      return false;
    }
    // лист, значит не должно быть указателей
    if (page.isLeaf && page.pointers.filter(elem => elem).length) {
      return false;
    }
    const leftValue = parent?.values[pointerIndex - 1];
    const rightValue = parent?.values[pointerIndex];
    let prevValue;
    for (let value of page.values) {
      // левое значение родителя должно быть больше всех других в этой странице
      if (leftValue && leftValue > value) {
        return false;
      }
      // правое значение родителя должно быть меньше всех других в этой странице
      if (rightValue && value > rightValue) {
        return false;
      }
      // значения в рамках страницы должны быть в порядке возрастания
      if (prevValue && prevValue > value) {
        return false;
      }
      prevValue = value;
    }
    for (let i = 0; i < page.pointers.length; ++i) {
      if (!page.pointers[i]) {
        break;
      }
      const flag = this.dfs(page.pointers[i], height, currentHeight, page, i);
      if (!flag) {
        return false;
      }
    }
    return true;
  }

  checkTree(tree) {
    let currentPage = tree.root;
    let height = 0;
    while (currentPage) {
      ++height;
      currentPage = currentPage.pointers[0];
    }
    this.treeRoot = tree.root;
    return this.dfs(tree.root, height);
  }
}

export class BTree {
  constructor(t = 3) {
    this._root = null;
    this._steps = [];
    this._t = t;
  }

  get root() {
    return this._root;
  }

  get steps() {
    const copy = this._steps;
    this._steps = [];
    return copy;
  }

  get t() {
    return this._t;
  }

  changeT(newT) {
    if (newT < 2 || newT > 7) {
      alert(GetTranslation('restrictionChecker_btreeDimensionality'));
      return;
    }
    if (newT !== this.t) {
      const newTree = new BTree(newT);
      const values = this.dfsWithUntying(this.root);
      for (let value of values) {
        newTree.insert(value);
      }
      this._root = newTree.root;
      this._t = newTree.t;
      newTree._root = null;
      this._steps.push({line: 0, graph: this.toDOT([]),});
    }
  }

  get isEmpty() {
    return this.root === null;
  }

  search(value) {
    let currentPage = this.root;
    this._steps.push({line: 2, graph: this.toDOT([currentPage])});
    while (currentPage) {
      const elemIndex = currentPage.findIndexElement(value);
      this._steps.push({line: 3, graph: this.toDOT([currentPage], elemIndex)});
      if (~elemIndex) {
        this._steps.push({line: 4, graph: this.toDOT([currentPage], elemIndex)});
        return {currentPage, elemIndex};
      }
      currentPage = currentPage.pointers[currentPage.findIndexPointer(value)];
      this._steps.push({line: 5, graph: this.toDOT([currentPage])});
    }
    this._steps.push({line: 7, graph: this.toDOT([currentPage])});
    return {currentPage: null, elemIndex: null};
  }

  doNormalizeAfterInsertion(page) {
    if (page.needNormalizeOverflowing()) {
      this._steps.push({line: 10, graph: this.toDOT([page]),});
      let {newLeft, medianValue, newRight, parent} = page.slice();
      this._steps.push({line: 11, graph: this.toDOT([newLeft, newRight]),});
      if (!parent) {
        this._root = new BTree_Node(this.t);
        parent = this._root;
        this._steps.push({line: 12, graph: this.toDOT([parent]),});
      }
      const elemIndex = parent.insertElement(medianValue);
      this._steps.push({line: 13, graph: this.toDOT([parent], elemIndex),});
      parent.setPointer(newLeft, elemIndex);
      parent.setPointer(newRight, elemIndex + 1);
      this._steps.push({line: 15, graph: this.toDOT([parent, newLeft, newRight]),});
      return parent;
    }
    return null;
  }

  normalizeAfterInsertion(page) {
    this._steps.push({line: 17, graph: this.toDOT([page]),});
    while (page) {
      page = this.doNormalizeAfterInsertion(page);
    }
  }

  insert(value) {
    let currentPage = this.root;
    if (!currentPage) {
      this._root = new BTree_Node(this.t);
      this._root.insertElement(value);
      this._steps.push({line: 3, graph: this.toDOT([this.root], 0),});
      return;
    }
    this._steps.push({line: 6, graph: this.toDOT([currentPage]),});
    while (!currentPage.isLeaf) {
      currentPage = currentPage.pointers[currentPage.findIndexPointer(value)];
      this._steps.push({line: 7, graph: this.toDOT([currentPage]),});
    }
    const index = currentPage.insertElement(value);
    this._steps.push({line: 8, graph: this.toDOT([currentPage], index),});

    this.normalizeAfterInsertion(currentPage);
  }

  doNormalizeAfterRemoval(page) {
    if (page.needNormalizeThin()) {
      this._steps.push({line: 14, graph: this.toDOT([page]),});
      const parentPage = page.parentPage;
      if (parentPage) {
        const curPageIndexInParentPointers = page.indexPointer;
        const nextPage = parentPage.pointers[curPageIndexInParentPointers + 1];
        const prevPage = parentPage.pointers[curPageIndexInParentPointers - 1];
        if (nextPage && nextPage.canSteal) {
          this._steps.push({line: 19, graph: this.toDOT([page, nextPage]),});
          page.values.push(parentPage.values[curPageIndexInParentPointers]);
          page.setPointer(...nextPage.pointers.splice(0, 1), page.values.length);
          nextPage.pointers.push(null);
          this._steps.push({line: 22, graph: this.toDOT([page, nextPage]),});
          parentPage.values[curPageIndexInParentPointers] = nextPage.values.shift();
          this._steps.push({line: 23, graph: this.toDOT([page, nextPage, parentPage]),});
        } else if (prevPage && prevPage.canSteal) {
          this._steps.push({line: 24, graph: this.toDOT([page, prevPage]),});
          page.insertElement(parentPage.values[curPageIndexInParentPointers - 1]);
          page.setPointer(...prevPage.pointers.splice(prevPage.values.length, 1, null), 0);
          this._steps.push({line: 26, graph: this.toDOT([page, prevPage]),});
          parentPage.values[curPageIndexInParentPointers - 1] = prevPage.values.pop();
          this._steps.push({line: 27, graph: this.toDOT([page, prevPage, parentPage]),});
        } else {
          let valuesIndex = 0, pointersIndex;
          if (prevPage) {
            this._steps.push({line: 30, graph: this.toDOT([page, prevPage]),});
            page.merge(prevPage, curPageIndexInParentPointers - 1);
            this._steps.push({line: 31, graph: this.toDOT([page, prevPage]),});
            valuesIndex = -1;
            pointersIndex = -1;
          } else {
            this._steps.push({line: 33, graph: this.toDOT([page, prevPage]),});
            page.merge(nextPage, curPageIndexInParentPointers, false);
            this._steps.push({line: 34, graph: this.toDOT([page, prevPage]),});
            pointersIndex = 1;
          }
          parentPage.values.splice(curPageIndexInParentPointers + valuesIndex, 1);
          this._steps.push({line: 37, graph: this.toDOT([page, parentPage]),});
          const removedPage = parentPage.pointers.splice(curPageIndexInParentPointers + pointersIndex, 1)[0];
          this._steps.push({line: 38, graph: this.toDOT([page]),});
          if (removedPage) {
            this._steps.push({line: 39, graph: this.toDOT([page]),});
            for (let item of removedPage.pointers.filter(elem => elem)) {
              page.pushPointer(item);
              this._steps.push({line: 40, graph: this.toDOT([page]),});
            }
          }
          parentPage.pointers.push(null);
          return parentPage;
        }
      }
    }
    return null;
  }

  normalizeAfterRemoval(page) {
    this._steps.push({line: 11, graph: this.toDOT([page]),});
    while (page) {
      page = this.doNormalizeAfterRemoval(page);
      this._steps.push({line: 11, graph: this.toDOT([page]),});
    }
    if (!this.root.values.length) {
      this._steps.push({line: 49, graph: this.toDOT([this.root]),});
      this._root = this.root.pointers[0];
      this._steps.push({line: 50, graph: this.toDOT([this.root]),});
      if (this.root) {
        this.root.parentPage = null;
        this._steps.push({line: 51, graph: this.toDOT([]),});
      }
    }
  }

  remove(value) {
    const {currentPage, elemIndex} = this.search(value);
    this._steps = [];
    this._steps.push({line: 1, graph: this.toDOT([currentPage], elemIndex),});
    if (!currentPage || elemIndex === null) {
      alert(GetTranslation('nothingToRemove'));
      this._steps.push({line: 2, graph: this.toDOT(),});
      return;
    }
    let pageToNormalize;
    if (currentPage.isLeaf) {
      this._steps.push({line: 4, graph: this.toDOT(),});
      currentPage.values.splice(elemIndex, 1);
      this._steps.push({line: 5, graph: this.toDOT(),});
      pageToNormalize = currentPage;
    } else {
      let rightInLeft = currentPage.pointers[elemIndex];
      this._steps.push({line: 8, graph: this.toDOT([currentPage, rightInLeft]),});
      while (!rightInLeft.isLeaf) {
        rightInLeft = rightInLeft.pointers[rightInLeft.values.length];
        this._steps.push({line: 9, graph: this.toDOT([currentPage, rightInLeft]),});
      }
      currentPage.values[elemIndex] = rightInLeft.values.pop();
      this._steps.push({line: 10, graph: this.toDOT([currentPage, rightInLeft]),});
      pageToNormalize = rightInLeft;
    }
    this.normalizeAfterRemoval(pageToNormalize);
    pageToNormalize.clearValuesFromNull();
  }

  toDotArc(parentPage, nameParentPage, childPage, index) {
    if (!childPage) {
      return '';
    }
    return `${' '.repeat(6)}"node${nameParentPage}":f${index * 2} -> node${nameParentPage}${index}:f1\n`;
  }

  toDotPage(page, namePage, activePages, activeIndex) {
    if (!page) {
      return '';
    }
    const colorPage = ~activePages.indexOf(page) ? ' COLOR="red"' : '';
    let label = `<<TABLE CELLSPACING="0" BORDER="0" CELLBORDER="1"${colorPage}>\n${' '.repeat(6)}<TR>\n${' '.repeat(8)}<TD PORT="f0"></TD>\n`;
    let i = 0;
    for (let j = 0; j < page.values.length; ++j) {
      const colorIndex = ~activePages.indexOf(page) && j === activeIndex ? ' COLOR="red"' : '';
      label += `${' '.repeat(8)}<TD PORT="f${++i}"><FONT${colorIndex}>${page.values[j]}</FONT></TD><TD PORT="f${++i}"></TD>`;
      label += '\n';
    }
    label += (' '.repeat(6) + '</TR>\n' + ' '.repeat(4) + '</TABLE>>');
    let dotGraph = `${' '.repeat(4)}node${namePage}[label=${label}]\n`;

    for (let i = 0; i < page.pointers.length; ++i) {
      const newName = `${namePage}${i}`;
      const pointer = page.pointers[i];
      dotGraph += this.toDotArc(page, namePage, pointer, i);
      dotGraph += this.toDotPage(pointer, newName, activePages, activeIndex);
    }
    return dotGraph;
  }

  toDOT(activePages = [], activeIndex) {
    let dotGraph = 'digraph{\n    node [shape=plaintext];\n';
    dotGraph += this.toDotPage(this.root, '0', activePages, activeIndex);
    dotGraph += '}';
    return dotGraph;
  }

  dfsWithUntying(page) {
    if (!page) {
      return [];
    }
    const answer = [];
    for (let i = 0; i < page.pointers.length; ++i) {
      answer.push(...this.dfsWithUntying(page.pointers[i]));
    }
    answer.push(...page.values);
    return answer;
  }

  importDFS(obj, t, parent = null) {
    if (!(obj?.values && Array.isArray(obj.values) && obj.values.every((item) => Number.isInteger(item)) && obj?.pointers && Array.isArray(obj.pointers))) {
      return {root: null, isValid: false,};
    }
    const page = new BTree_Node(t, obj.values);
    page.parentPage = parent;
    const pointers = [];
    for (let pointer of obj.pointers) {
      const {root, isValid} = this.importDFS(pointer, t, page);
      if (isValid) pointers.push(root);
      else return {root: null, isValid: false,};
    }
    page.pointers.unshift(...pointers);
    page.pointers.splice(2 * t);
    return {root: page, isValid: true,};
  }

  importState(stringState) {
    const state = JSON.parse(stringState);
    if (state?.name === 'B Tree' && state?.t && Number.isInteger(state.t) && state?.nodes) {
      const oldRoot = this.root, oldT = this.t;
      const {root, isValid} = this.importDFS(state.nodes, state.t);
      if (!isValid) {
        alert(GetTranslation('corruptedFile'));
      } else {
        this._root = root;
        this._t = state.t;
        const helper = new BTree_TestHelper();
        if (!helper.checkTree(this)) {
          this._root = oldRoot;
          this._t = oldT;
          alert(GetTranslation('corruptedFile'));
        }
      }
    } else {
      alert(GetTranslation('wrongFile'));
    }
    this._steps.push({line: 0, graph: this.toDOT(),});
  }

  exportDFS(page) {
    const values = page.values;
    const pointers = [];
    for (let pointer of page.pointers.filter(item => item)) {
      pointers.push(this.exportDFS(pointer));
    }
    return {values, pointers,};
  }

  exportState() {
    if (!this.root) {
      alert('Nothing to export');
      return;
    }
    const result = JSON.stringify({name: 'B Tree', t: this.t, nodes: this.exportDFS(this.root),});
    saveAs(new Blob([result], {type: 'text/plain;charset=utf-8'}), 'BTree.json');
  }
}