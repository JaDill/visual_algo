import {GetTranslation} from '../translations';
import {saveAs} from 'file-saver';

const RED = 'red';
const BLACK = 'black';

export class RedBlackTree_Node {
  constructor(key, leftChildNode = null, rightChildNode = null, parentNode = null, color = BLACK) {
    this._parentNode = parentNode;
    this._key = key;
    this._color = color;
    this._leftChildNode = leftChildNode;
    this._rightChildNode = rightChildNode;
    if (this._leftChildNode !== null) {
      leftChildNode._parentNode = this;
    }
    if (this._rightChildNode !== null) {
      rightChildNode._parentNode = this;
    }
  }

  get leftChildNode() {
    return this._leftChildNode;
  }

  get rightChildNode() {
    return this._rightChildNode;
  }

  get parentNode() {
    return this._parentNode;
  }

  get isBlack() {
    return this._color === BLACK;
  }

  get isRed() {
    return this._color === RED;
  }

  get isLeftChild() {
    if (!this._parentNode) {
      return false;
    }
    return (this._parentNode._leftChildNode === this);
  }

  get isRightChild() {
    if (!this._parentNode) {
      return false;
    }
    return (this._parentNode._rightChildNode === this);
  }

  get key() {
    return this._key;
  }

  setLeft(node) {
    if (this._leftChildNode === node) {
      return null;
    }
    if (node) {
      if (node._parentNode) {
        if (node._parentNode._leftChildNode === node) {
          node._parentNode._leftChildNode = null;
        } else {
          node._parentNode._rightChildNode = null;
        }
      }
      node._parentNode = this;
    }
    const prevLeft = this._leftChildNode;
    this._leftChildNode = node;
    if (prevLeft) {
      prevLeft._parentNode = null;
    }
    return prevLeft;
  }

  setRight(node) {
    if (this._rightChildNode === node) {
      return null;
    }
    if (node) {
      if (node._parentNode) {
        if (node._parentNode._leftChildNode === node) {
          node._parentNode._leftChildNode = null;
        } else {
          node._parentNode._rightChildNode = null;
        }
      }
      node._parentNode = this;
    }
    const prevRight = this._rightChildNode;
    this._rightChildNode = node;
    if (prevRight) {
      prevRight._parentNode = null;
    }
    return prevRight;
  }

  setBlack() {
    this._color = BLACK;
  }

  setRed() {
    this._color = RED;
  }
}

export class RedBlackTree_TestHelper {
  dfs(node, countBlack, currentCountBlack, parent = null, isLeftChild = true) {
    if (!node) {
      return currentCountBlack === countBlack;
    }

    if (node.isBlack) {
      ++currentCountBlack;
    } else if ((node.leftChildNode && node.leftChildNode.isRed) ||
      (node.rightChildNode && node.rightChildNode.isRed)) {
      return false;
    }
    if (parent && ((isLeftChild && node.key > parent.key) || (!isLeftChild && node.key <= parent.key))) {
      return false;
    }

    const l = this.dfs(node.leftChildNode, countBlack, currentCountBlack, node);
    const r = this.dfs(node.rightChildNode, countBlack, currentCountBlack, node, false);
    return (l && r);
  }

  checkTree(tree) {
    let currentNode = tree.root;
    if (currentNode && currentNode.isRed) {
      return false;
    }
    let countBlack = 0;
    while (currentNode) {
      if (currentNode.isBlack) {
        ++countBlack;
      }
      currentNode = currentNode.leftChildNode;
    }
    return this.dfs(tree.root, countBlack, 0);
  }
}

export class RedBlackTree {
  constructor() {
    this._root = null;
    this._steps = [];
  }

  get steps() {
    const copy = this._steps;
    this._steps = [];
    return copy;
  }

  rebalanceAfterRemoving(node) {
    if (node.isLeftChild) {
      this._steps.push({line: 18, graph: this.toDOT(node),});
      let brother = node.parentNode.rightChildNode;
      if (brother.isRed) {
        this._steps.push({line: 19, graph: this.toDOT(node),});
        brother.setBlack();
        node.parentNode.setRed();
        this._steps.push({line: 20, graph: this.toDOT(node),});
        this.rotateLeft(node.parentNode);
        this._steps.push({line: 21, graph: this.toDOT(node),});
        brother = node.parentNode.rightChildNode;
      }

      let isLCBBlack = !brother.leftChildNode || brother.leftChildNode.isBlack;
      let isRCBBlack = !brother.rightChildNode || brother.rightChildNode.isBlack;
      if (isLCBBlack && isRCBBlack) {
        this._steps.push({line: 23, graph: this.toDOT(node),});
        brother.setRed();
        this._steps.push({line: 24, graph: this.toDOT(node),});
        node = node.parentNode;
        this._steps.push({line: 25, graph: this.toDOT(node),});
      } else {
        this._steps.push({line: 26, graph: this.toDOT(node),});
        if (isRCBBlack) {
          this._steps.push({line: 27, graph: this.toDOT(node),});
          brother.leftChildNode.setBlack();
          brother.setRed();
          this._steps.push({line: 28, graph: this.toDOT(node),});
          this.rotateRight(brother);
          this._steps.push({line: 29, graph: this.toDOT(node),});
          brother = node.parentNode.rightChildNode;
        }
        brother._color = node.parentNode._color;
        node.parentNode.setBlack();
        if (brother.rightChildNode) {
          brother.rightChildNode.setBlack();
        }
        this._steps.push({line: 31, graph: this.toDOT(node),});
        this.rotateLeft(node.parentNode);
        this._steps.push({line: 32, graph: this.toDOT(node),});
        node = this._root;
        this._steps.push({line: 33, graph: this.toDOT(node),});
      }
    } else {
      this._steps.push({line: 35, graph: this.toDOT(node),});
      let brother = node.parentNode.leftChildNode;
      if (brother.isRed) {
        this._steps.push({line: 36, graph: this.toDOT(node),});
        brother.setBlack();
        node.parentNode.setRed();
        this._steps.push({line: 37, graph: this.toDOT(node),});
        this.rotateRight(node.parentNode);
        this._steps.push({line: 38, graph: this.toDOT(node),});
        brother = node.parentNode.leftChildNode;
      }

      let isLCBBlack = !brother.leftChildNode || brother.leftChildNode.isBlack;
      let isRCBBlack = !brother.rightChildNode || brother.rightChildNode.isBlack;
      if (isLCBBlack && isRCBBlack) {
        this._steps.push({line: 40, graph: this.toDOT(node),});
        brother.setRed();
        this._steps.push({line: 41, graph: this.toDOT(node),});
        node = node.parentNode;
        this._steps.push({line: 42, graph: this.toDOT(node),});
      } else {
        this._steps.push({line: 43, graph: this.toDOT(node),});
        if (isLCBBlack) {
          this._steps.push({line: 44, graph: this.toDOT(node),});
          brother.rightChildNode.setBlack();
          brother.setRed();
          this._steps.push({line: 45, graph: this.toDOT(node),});
          this.rotateLeft(brother);
          this._steps.push({line: 46, graph: this.toDOT(node),});
          brother = node.parentNode.leftChildNode;
        }
        brother._color = node.parentNode._color;
        node.parentNode.setBlack();
        if (brother.leftChildNode) {
          brother.leftChildNode.setBlack();
        }
        this._steps.push({line: 48, graph: this.toDOT(node),});
        this.rotateRight(node.parentNode);
        this._steps.push({line: 49, graph: this.toDOT(node),});
        node = this._root;
        this._steps.push({line: 50, graph: this.toDOT(node),});
      }
    }
    return node;
  }

  fixRemoving(node) {
    ///пока текущий элемент - черный
    this._steps.push({line: 17, graph: this.toDOT(node),});
    while (node !== this._root && node.isBlack) {
      node = this.rebalanceAfterRemoving(node);
      this._steps.push({line: 17, graph: this.toDOT(node),});
    }
    node.setBlack();
    this._steps.push({line: 53, graph: this.toDOT(node),});
  }

  remove(key) {
    const currentNode = this.search(key);
    this._steps = [];
    this._steps.push({line: 1, graph: this.toDOT(currentNode),});
    if (!currentNode) {
      this._steps.push({line: 2, graph: this.toDOT(),});
      alert(GetTranslation('nothingToRemove'));
      return;
    }
    let temp;
    // если у ячейки, которую нужно удалить, не два ребенка, то она и будет удаляться
    if (!currentNode.leftChildNode || !currentNode.rightChildNode) {
      temp = currentNode;
      this._steps.push({line: 3, graph: this.toDOT(temp),});
    } else {
      // если два ребенка то удалить нам нужно будет самую минимальную ячейку в правом поддереве нашей ноды
      temp = currentNode.rightChildNode;
      this._steps.push({line: 4, graph: this.toDOT(temp),});
      while (temp.leftChildNode) {
        temp = temp.leftChildNode;
        this._steps.push({line: 4, graph: this.toDOT(temp),});
      }
    }
    let child = null;
    // запоминаем ребенка которого надо будет сдвинуть после удаления
    if (temp.leftChildNode) {
      child = temp.leftChildNode;
      temp._leftChildNode = null;
      this._steps.push({line: 5, graph: this.toDOT(temp),});
    } else if (temp.rightChildNode) {
      child = temp.rightChildNode;
      temp._rightChildNode = null;
      this._steps.push({line: 6, graph: this.toDOT(temp),});
    }

    // если у удаляемой ноды нед детей, создаем псевдо-ребенка, чтобы от него производить балансировку
    if (!child && temp !== this._root) {
      child = new RedBlackTree_Node(-1);
      this._steps.push({line: 7, graph: this.toDOT(child),});
    }

    // изолируем ноду, которую будем удалять
    const parent = temp.parentNode;
    if (child) {
      child._parentNode = parent;
      this._steps.push({line: 9, graph: this.toDOT(child),});
    }

    if (parent) {
      if (temp.isLeftChild) {
        parent._leftChildNode = child;
        this._steps.push({line: 11, graph: this.toDOT(parent),});
      } else {
        parent._rightChildNode = child;
        this._steps.push({line: 12, graph: this.toDOT(parent),});
      }
      temp._parentNode = null;
    } else {
      this._root = child;
      this._steps.push({line: 14, graph: this.toDOT(child),});
    }

    // если нодой, которую нужно удалить, является минимальная в правом поддереве, то меняем значения этой ноды и изначальной
    if (temp !== currentNode) {
      currentNode._key = temp.key;
      this._steps.push({line: 15, graph: this.toDOT(currentNode),});
    }

    if (child && temp.isBlack) {
      this._steps.push({line: 16, graph: this.toDOT(child),});
      this.fixRemoving(child);
    }

    ///удаляем псевдо-ребенка
    if (child && child.key === -1) {
      this._steps.push({line: 54, graph: this.toDOT(child),});
      if (child.parentNode) {
        if (child.isRightChild) {
          child.parentNode._rightChildNode = null;
        } else {
          child.parentNode._leftChildNode = null;
        }
      }

      child._parentNode = null;
      this._steps.push({line: 54, graph: this.toDOT(child),});
    }
  }

  search(key) {
    let currentNode = this.root;
    while (currentNode && currentNode.key !== key) {
      this._steps.push({line: 2, graph: this.toDOT(currentNode),});
      if (key < currentNode.key) {
        currentNode = currentNode.leftChildNode;
        this._steps.push({line: 4, graph: this.toDOT(currentNode),});
      } else {
        currentNode = currentNode.rightChildNode;
        this._steps.push({line: 6, graph: this.toDOT(currentNode),});
      }
    }
    this._steps.push({line: 7, graph: this.toDOT(currentNode),});
    return currentNode;
  }

  get isEmpty() {
    return !this._root;
  }

  get root() {
    return this._root;
  }

  insertNewBstEl(key) {
    if (this.isEmpty) {
      this._root = new RedBlackTree_Node(key);
      this._steps.push({line: 2, graph: this.toDOT(this.root),});
      return this.root;
    }
    let prev;
    let current = this.root;
    this._steps.push({line: 4, graph: this.toDOT(current),});
    while (current && current.key !== key) {
      prev = current;
      if (key < current.key) {
        current = current.leftChildNode;
      } else {
        current = current.rightChildNode;
      }
      if (current) {
        this._steps.push({line: 4, graph: this.toDOT(current),});
      }
    }
    if (current && current.key === key) {
      alert(GetTranslation('reinsertError'));
      return null;
    }
    current = new RedBlackTree_Node(key, null, null, null, RED);
    if (key < prev.key) {
      prev.setLeft(current);
      this._steps.push({line: 7, graph: this.toDOT(current),});
    } else {
      prev.setRight(current);
      this._steps.push({line: 9, graph: this.toDOT(current),});
    }
    return current;
  }

  rotateLeft(node) {
    const y = node.rightChildNode;
    if (!y) {
      console.log('невозможно повернуть, нет правого ребёнка');
      return null;
    }
    y._parentNode = node.parentNode;
    if (node.parentNode) {
      if (node.isLeftChild) {
        node.parentNode._leftChildNode = y;
      } else {
        node.parentNode._rightChildNode = y;
      }
    } else {
      this._root = y;
    }
    node._rightChildNode = y._leftChildNode;
    if (y.leftChildNode) {
      y.leftChildNode._parentNode = node;
    }
    node._parentNode = y;
    y._leftChildNode = node;
  }

  rotateRight(node) {
    const y = node.leftChildNode;
    if (!y) {
      console.log('невозможно повернуть, нет левого ребёнка');
      return null;
    }
    y._parentNode = node.parentNode;
    if (node.parentNode) {
      if (node.isLeftChild) {
        node.parentNode._leftChildNode = y;
      } else {
        node.parentNode._rightChildNode = y;
      }
    } else {
      this._root = y;
    }
    node._leftChildNode = y._rightChildNode;
    if (y.rightChildNode) {
      y.rightChildNode._parentNode = node;
    }
    node._parentNode = y;
    y._rightChildNode = node;
  }

  rebalanceAfterInsertion(node) {
    let uncle;
    if (node.parentNode.isLeftChild) {
      uncle = node.parentNode.parentNode.rightChildNode;
    } else {
      uncle = node.parentNode.parentNode.leftChildNode;
    }
    this._steps.push({line: 11, graph: this.toDOT(uncle),});
    if (uncle && uncle.isRed) {
      this._steps.push({line: 12, graph: this.toDOT(uncle),});
      node.parentNode.setBlack();
      uncle.setBlack();
      node.parentNode.parentNode.setRed();
      this._steps.push({line: 13, graph: this.toDOT(node),});
      node = node.parentNode.parentNode;
      this._steps.push({line: 14, graph: this.toDOT(node),});
    } else {
      this._steps.push({line: 15, graph: this.toDOT(uncle),});
      if (node.isLeftChild) {
        this._steps.push({line: 16, graph: this.toDOT(node),});
        node.parentNode.setBlack();
        node.parentNode.parentNode.setRed();
        this._steps.push({line: 17, graph: this.toDOT(node),});
        if (node.parentNode.isLeftChild) {
          this._steps.push({line: 18, graph: this.toDOT(node.parentNode),});
          this.rotateRight(node.parentNode.parentNode);
          this._steps.push({line: 19, graph: this.toDOT(node),});
        } else {
          this._steps.push({line: 20, graph: this.toDOT(node.parentNode),});
          this.rotateLeft(node.parentNode.parentNode);
          this._steps.push({line: 21, graph: this.toDOT(node),});
        }
      } else {
        this._steps.push({line: 22, graph: this.toDOT(node),});
        node = node.parentNode;
        this._steps.push({line: 23, graph: this.toDOT(node),});
        this.rotateLeft(node);
        this._steps.push({line: 24, graph: this.toDOT(node),});
      }
    }
    return node;
  }

  fixInsertion(node) {
    if (!node.parentNode) {
      return;
    }

    while (node !== this._root && node.parentNode.isRed) {
      this._steps.push({line: 10, graph: this.toDOT(node),});
      node = this.rebalanceAfterInsertion(node);
    }
    this._root.setBlack();
    this._steps.push({line: 29, graph: this.toDOT(this.root),});
  }

  insert(key) {
    const newNode = this.insertNewBstEl(key);
    if (newNode) {
      this.fixInsertion(newNode);
    }
  }

  toDotArc(parentNode, childNode, isLeft) {
    let childDotDesc, dotGraph = '';
    if (childNode) {
      childDotDesc = `${childNode.key}`;
    } else {
      childDotDesc = `LEAF${isLeft ? 'l' : 'r'}${parentNode.key}`;
      dotGraph += `${childDotDesc}  [label="nil",width=0.3,height=0.2,shape=box,fillcolor=black]\n`;
    }
    dotGraph += `    ${parentNode.key} -> ${childDotDesc}\n`;
    return dotGraph;
  }

  toDotNode(node, activeNode) {
    if (!node) {
      return '';
    }
    let dotGraph = `    ${node.key} [fillcolor=${node._color}${node === activeNode ? ',peripheries=2' : ''}]\n`;

    dotGraph += this.toDotArc(node, node.leftChildNode, true);
    dotGraph += this.toDotArc(node, node.rightChildNode, false);

    dotGraph += this.toDotNode(node.leftChildNode, activeNode);
    dotGraph += this.toDotNode(node.rightChildNode, activeNode);
    return dotGraph;
  }

  toDOT(activeNode) {
    let dotGraph = 'digraph{\n    node [width=0.5,fontcolor=white,style=filled];\n';
    dotGraph += this.toDotNode(this._root, activeNode);
    dotGraph += '}';
    return dotGraph;
  }

  importDFS(obj, parent = null) {
    if (!(obj?.value && Number.isInteger(obj.value) && obj?.color && (obj.color === RED || obj.color === BLACK))) {
      return {root: null, isValid: false,};
    }
    const node = new RedBlackTree_Node(obj.value);
    node._parentNode = parent;
    node._color = obj.color === BLACK ? BLACK : RED;
    if (obj?.leftChild) {
      const {root, isValid} = this.importDFS(obj.leftChild, node);
      if (isValid) node._leftChildNode = root;
      else return {root: null, isValid: false,};
    }
    if (obj?.rightChild) {
      const {root, isValid} = this.importDFS(obj.rightChild, node);
      if (isValid) node._rightChildNode = root;
      else return {root: null, isValid: false,};
    }
    return {root: node, isValid: true,};
  }

  importState(stringState) {
    const state = JSON.parse(stringState);
    if (state?.name === 'Red Black Tree' && state?.nodes) {
      const oldRoot = this.root;
      const {root, isValid} = this.importDFS((state.nodes));
      if (!isValid) {
        alert(GetTranslation('corruptedFile'));
      } else {
        this._root = root;
        const helper = new RedBlackTree_TestHelper();
        if (!helper.checkTree(this)) {
          this._root = oldRoot;
          alert(GetTranslation('corruptedFile'));
        }
      }
    } else {
      alert(GetTranslation('wrongFile'));
    }
    this._steps.push({line: 0, graph: this.toDOT(),});
  }

  exportDFS(node) {
    const leftChild = node.leftChildNode ? this.exportDFS(node.leftChildNode) : null;
    const rightChild = node.rightChildNode ? this.exportDFS(node.rightChildNode) : null;
    return {value: node.key, color: node._color, leftChild, rightChild};
  }

  exportState() {
    if (!this.root) {
      alert('Nothing to export');
      return;
    }
    const result = JSON.stringify({name: 'Red Black Tree', nodes: this.exportDFS(this.root),});
    saveAs(new Blob([result], {type: 'text/plain;charset=utf-8'}), 'RBTree.json');
  }
}