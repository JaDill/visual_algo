import {GetTranslation} from '../translations';
import {saveAs} from 'file-saver';

export class Treap_Node {
  constructor(key, priority, left = null, right = null) {
    this._key = key;
    this._priority = priority;
    this._left = left;
    if (left) {
      left._parent = this;
    }
    this._right = right;
    if (right) {
      right._parent = this;
    }
    this._parent = null;
  }

  get key() {
    return this._key;
  }

  get priority() {
    return this._priority;
  }

  get leftChildNode() {
    return this._left;
  }

  get rightChildNode() {
    return this._right;
  }

  get parentNode() {
    return this._parent;
  }

  set leftChildNode(node) {
    this._left = node;
    if (node) {
      node._parent = this;
    }
  }

  set rightChildNode(node) {
    this._right = node;
    if (node) {
      node._parent = this;
    }
  }

  set parentNode(node) {
    this._parent = node;
  }

  untyingParent() {
    if (this.parentNode) {
      if (this.parentNode.leftChildNode === this) {
        this.parentNode.leftChildNode = null;
      } else if (this.parentNode.rightChildNode === this) {
        this.parentNode.rightChildNode = null;
      }
      this.parentNode = null;
    }
  }

  destroy() {
    this.untyingParent();
    if (this._left) {
      this._left._parent = null;
    }
    if (this._right) {
      this._right._parent = null;
    }
    this._left = this._right = null;
  }
}

export class Treap_TestHelper {
  dfs(node, isLeftChild = true) {
    if (!node) {
      return true;
    }
    if (node.parentNode) {
      // приоритет у детей не может быть больше родительского приоритета
      if (node.priority > node.parentNode.priority) {
        return false;
      }
      // ключи слева должны быть меньше родитеского, а ключи справа - не меньше родительского
      if ((isLeftChild && node.key > node.parentNode.key) || (!isLeftChild && node.key <= node.parentNode.key)) {
        return false;
      }
    }
    const l = this.dfs(node.leftChildNode);
    const r = this.dfs(node.rightChildNode, false);
    return (l && r);
  }

  checkTree(tree) {
    return this.dfs(tree.root);
  }
}

export class Treap {
  constructor() {
    this._root = null;
    this._steps = [];
    this._nodes = [];
  }

  get isEmpty() {
    return this.root === null;
  }

  get root() {
    return this._root;
  }

  get steps() {
    const copy = this._steps;
    this._steps = [];
    return copy;
  }

  destroyNode(node) {
    node.destroy();
    while (~this._nodes.indexOf(node)) {
      this._nodes.splice(this._nodes.indexOf(node), 1);
    }
  }

  createNode(key, priority, leftChildNode = null, rightChildNode = null) {
    const newNode = new Treap_Node(key, priority, leftChildNode, rightChildNode);
    this._nodes.push(newNode);
    return newNode;
  }

  merge(leftRootNode, rightRootNode) {
    if (!leftRootNode) {
      if (rightRootNode) rightRootNode.untyingParent();
      this._steps.push({line: 2, graph: this.toDOT([leftRootNode, rightRootNode]),});
      return rightRootNode;
    }
    if (!rightRootNode) {
      if (leftRootNode) leftRootNode.untyingParent();
      this._steps.push({line: 3, graph: this.toDOT([leftRootNode, rightRootNode]),});
      return leftRootNode;
    }

    let result;
    if (leftRootNode.priority > rightRootNode.priority) {
      this._steps.push({line: 4, graph: this.toDOT(),});
      const newRightRootNode = this.merge(leftRootNode.rightChildNode, rightRootNode);
      this._steps.push({line: 5, graph: this.toDOT([leftRootNode.leftChildNode, leftRootNode, newRightRootNode]),});
      leftRootNode.rightChildNode = newRightRootNode;
      result = leftRootNode;
      this._steps.push({line: 6, graph: this.toDOT([result]),});
    } else {
      this._steps.push({line: 7, graph: this.toDOT(),});
      const newLeftRootNode = this.merge(leftRootNode, rightRootNode.leftChildNode);
      this._steps.push({line: 8, graph: this.toDOT([newLeftRootNode, rightRootNode, rightRootNode.rightChildNode]),});
      rightRootNode.leftChildNode = newLeftRootNode;
      result = rightRootNode;
      this._steps.push({line: 9, graph: this.toDOT([result]),});
    }
    return result;
  }

  split(key, node) {
    let L = null, R = null, newTree = null;
    // this._steps.push({line: 13, graph: this.toDOT([node], node),});
    if (node) {
      // this._steps.push({line: 14, graph: this.toDOT([node], node),});
      if (node.key <= key) {
        // this._steps.push({line: 15, graph: this.toDOT([node], node),});
        if (node.rightChildNode) {
          // this._steps.push({line: 16, graph: this.toDOT([node], node),});
          const ans = this.split(key, node.rightChildNode);
          newTree = ans.L;
          R = ans.R;
          this._steps.push({line: 17, graph: this.toDOT([node.leftChildNode, newTree, R]),});
        }
        node.rightChildNode = newTree;
        L = node;
        this._steps.push({line: 19, graph: this.toDOT([L, R]),});
      } else {
        // this._steps.push({line: 20, graph: this.toDOT([node], node),});
        if (node.leftChildNode) {
          // this._steps.push({line: 21, graph: this.toDOT([node]),});
          const ans = this.split(key, node.leftChildNode);
          L = ans.L;
          newTree = ans.R;
          this._steps.push({line: 22, graph: this.toDOT([L, newTree, node.rightChildNode]),});
        }
        R = node;
        node.leftChildNode = newTree;
        this._steps.push({line: 24, graph: this.toDOT([L, R]),});
      }
    }
    // this._steps.push({line: 27, graph: this.toDOT([L, R],),});
    if (L) {
      L.untyingParent();
    }
    if (R) {
      R.untyingParent();
    }
    return {L, R};
  }

  insert(key) {
    if (this.search(key)?.key === key) {
      alert(GetTranslation('reinsertError'));
      return;
    }
    this._steps = [];
    this._steps.push({line: 29, graph: this.toDOT(),});
    const {L, R} = this.split(key, this.root);
    this._steps.push({line: 30, graph: this.toDOT([L, R]),});
    const newNode = this.createNode(key, this.randomPriority());
    this._steps.push({line: 31, graph: this.toDOT([L, newNode, R]),});
    this._root = this.merge(this.merge(L, newNode), R);
    this._steps.push({line: 32, graph: this.toDOT(),});
  }

  remove(key) {
    this._steps.push({line: 30, graph: this.toDOT(),});
    const {L, R} = this.split(key - 1, this.root);
    this._steps.push({line: 30, graph: this.toDOT([L, R]),});
    const {L: RL, R: RR} = this.split(key, R);
    this._steps.push({line: 31, graph: this.toDOT([L, RL, RR]),});
    this.destroyNode(RL);
    this._root = this.merge(L, RR);
    this._steps.push({line: 32, graph: this.toDOT(),});
  }

  randomPriority() {
    return Math.floor(Math.random() * (1000 - 1) + 1);
  }

  search(key) {
    let currentNode = this.root;
    while (currentNode && currentNode.key !== key) {
      this._steps.push({line: 2, graph: this.toDOT([currentNode]),});
      if (key < currentNode.key) {
        currentNode = currentNode.leftChildNode;
        this._steps.push({line: 4, graph: this.toDOT([currentNode]),});
      } else {
        currentNode = currentNode.rightChildNode;
        this._steps.push({line: 6, graph: this.toDOT([currentNode]),});
      }
    }
    this._steps.push({line: 9, graph: this.toDOT([currentNode]),});
    return currentNode;
  }

  toDotArc(parentNode, childNode, isLeft) {
    let childDotDesc, dotGraph = '';
    if (childNode) {
      childDotDesc = `${childNode.key}`;
    } else {
      childDotDesc = `LEAF${isLeft ? 'l' : 'r'}${parentNode.key}`;
      dotGraph += `${childDotDesc}  [label="nil",width=0.3,height=0.2,shape=box,fillcolor=black]\n`;
    }
    dotGraph += `    ${parentNode.key} -> ${childDotDesc}\n`;
    return dotGraph;
  }

  toDotNode(set, node, activeNodes) {
    if (!node || set.has(node.key)) {
      return '';
    }
    set.add(node.key);

    let dotGraph = `    ${node.key} [label="${node.key}/${node.priority}",fillcolor=black${~activeNodes.indexOf(node) ? ',peripheries=2' : ''}]\n`;

    dotGraph += this.toDotArc(node, node.leftChildNode, true);
    dotGraph += this.toDotArc(node, node.rightChildNode, false);

    dotGraph += this.toDotNode(set, node.leftChildNode, activeNodes);
    dotGraph += this.toDotNode(set, node.rightChildNode, activeNodes);
    return dotGraph;
  }

  toDOT(activeNodes = []) {
    let dotGraph = 'digraph{graph [ordering="out"];\n    node [width=0.5,fontcolor=white,style=filled];\n';
    let startNodes = [...this._nodes];
    const temp = new Set();
    for (let t of startNodes) {
      temp.add(t?.leftChildNode?.key);
      temp.add(t?.rightChildNode?.key);
    }
    startNodes = startNodes.filter(elem => elem && !temp.has(elem.key));
    startNodes.sort((a, b) => a.key < b.key ? -1 : 1);
    const set = new Set();
    for (let startNode of startNodes) {
      if (!set.has(startNode.key)) {
        dotGraph += 'subgraph{\n';
        dotGraph += this.toDotNode(set, startNode, activeNodes);
        dotGraph += '}\n';
      }
    }
    dotGraph += '}';
    return dotGraph;
  }

  importDFS(obj) {
    if (!(obj?.value && Number.isInteger(obj.value) && obj?.priority && Number.isInteger(obj.priority))) {
      return {root: null, isValid: false,};
    }
    const node = this.createNode(obj.value, obj.priority);
    if (obj?.leftChild) {
      const {root, isValid} = this.importDFS(obj.leftChild, node);
      if (isValid) node.leftChildNode = root;
      else return {root: null, isValid: false,};
    }
    if (obj?.rightChild) {
      const {root, isValid} = this.importDFS(obj.rightChild, node);
      if (isValid) node.rightChildNode = root;
      else return {root: null, isValid: false,};
    }
    return {root: node, isValid: true,};
  }

  importState(stringState) {
    const state = JSON.parse(stringState);
    if (state?.name === 'Treap' && state?.nodes) {
      const oldRoot = this.root;
      const oldNodes = this._nodes;
      this._nodes = [];
      const {root, isValid} = this.importDFS(state.nodes);
      if (!isValid) {
        alert(GetTranslation('corruptedFile'));
      } else {
        this._root = root;
        const helper = new Treap_TestHelper();
        if (!helper.checkTree(this)) {
          this._root = oldRoot;
          this._nodes = oldNodes;
          alert(GetTranslation('corruptedFile'));
        }
      }
    } else {
      alert(GetTranslation('wrongFile'));
    }
    this._steps.push({line: 0, graph: this.toDOT(),});
  }

  exportDFS(node) {
    const leftChild = node.leftChildNode ? this.exportDFS(node.leftChildNode) : null;
    const rightChild = node.rightChildNode ? this.exportDFS(node.rightChildNode) : null;
    return {value: node.key, priority: node.priority, leftChild, rightChild};
  }

  exportState() {
    if (!this.root) {
      alert('Nothing to export');
      return;
    }
    const result = JSON.stringify({name: 'Treap', nodes: this.exportDFS(this.root),});
    saveAs(new Blob([result], {type: 'text/plain;charset=utf-8'}), 'Treap.json');
  }
}